#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <WS2812FX.h>
#include <ESP8266WebServer.h>

#include "settings.h"


extern const char index_html[];
extern const char main_js[];

// QUICKFIX...See https://github.com/esp8266/Arduino/issues/263
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

const char* ssid = STASSID;
const char* password = STAPSK;
int i = 0;

uint8_t brightness = DEFAULT_BRIGHTNESS;
uint8_t power = DEFAULT_POWER;
uint32_t color = DEFAULT_COLOR;

unsigned long auto_last_change = 0;
unsigned long last_wifi_check_time = 0;
String data = "";
uint8_t myModes[] = { }; // *** optionally create a custom list of effect/mode numbers
boolean auto_cycle = false;

WS2812FX ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ400);
ESP8266WebServer server(HTTP_PORT);

void setup() {
	Serial.begin(115200);
	Serial.println("Booting");
	WiFi.mode(WIFI_STA);
	WiFi.hostname("salt-lamp");
	WiFi.begin(ssid, password);
	while (WiFi.waitForConnectResult() != WL_CONNECTED) {
		Serial.println("Connection Failed! Rebooting...");
		delay(5000);
		ESP.restart();
	}

//	pinMode(LED_PIN, OUTPUT);
//	digitalWrite(LED_PIN, LOW);

//	pinMode(LED_BUILTIN, OUTPUT);
//  digitalWrite(ESP8266_LED, HIGH);

// Port defaults to 8266
// ArduinoOTA.setPort(8266);

// Hostname defaults to esp8266-[ChipID]
	ArduinoOTA.setHostname("salt-lamp");


	// No authentication by default
	// ArduinoOTA.setPassword("admin");

	// Password can be set with it's md5 value as well
	// MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
	// ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

	ArduinoOTA.onStart([]() {
		String type;
		if (ArduinoOTA.getCommand() == U_FLASH) {
			type = "sketch";
		} else { // U_SPIFFS
				type = "filesystem";
			}

			// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
			Serial.println("Start updating " + type);
		});
	ArduinoOTA.onEnd([]() {
		Serial.println("\nEnd");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		Serial.printf("Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) {
			Serial.println("Auth Failed");
		} else if (error == OTA_BEGIN_ERROR) {
			Serial.println("Begin Failed");
		} else if (error == OTA_CONNECT_ERROR) {
			Serial.println("Connect Failed");
		} else if (error == OTA_RECEIVE_ERROR) {
			Serial.println("Receive Failed");
		} else if (error == OTA_END_ERROR) {
			Serial.println("End Failed");
		}
	});
	ArduinoOTA.begin();
	Serial.println("Ready");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	data.reserve(5000);

	ws2812fx.init();

	if (power) {
		ws2812fx.setBrightness(brightness);
	} else {
		ws2812fx.setBrightness(0);
	}
	ws2812fx.setColor(color);
	ws2812fx.setSpeed(DEFAULT_SPEED);
	ws2812fx.setMode(DEFAULT_MODE);

	data_setup();

	ws2812fx.start();

	Serial.println("Wifi setup");
	wifi_setup();

	Serial.println("HTTP server setup");
	server.on("/", srv_handle_index_html);
	server.on("/main.js", srv_handle_main_js);
	server.on("/data", srv_handle_data);
	server.on("/set", srv_handle_set);
	server.onNotFound(srv_handle_not_found);
	server.begin();
	Serial.println("HTTP server started.");

	Serial.println("ready!");
}

void loop() {
	unsigned long now = millis();

	server.handleClient();
	ws2812fx.service();
	ArduinoOTA.handle();

	if (now - last_wifi_check_time > WIFI_TIMEOUT) {
		Serial.print("Checking WiFi... ");
		if (WiFi.status() != WL_CONNECTED) {
			Serial.println("WiFi connection lost. Reconnecting...");
			wifi_setup();
		} else {
			Serial.println("OK");
		}
		last_wifi_check_time = now;
	}

	if (auto_cycle && (now - auto_last_change > 10000)) { // cycle effect mode every 10 seconds
		uint8_t next_mode = (ws2812fx.getMode() + 1) % ws2812fx.getModeCount();
		if (sizeof(myModes) > 0) { // if custom list of modes exists
			for (uint8_t i = 0; i < sizeof(myModes); i++) {
				if (myModes[i] == ws2812fx.getMode()) {
					next_mode =
							((i + 1) < sizeof(myModes)) ?
									myModes[i + 1] : myModes[0];
					break;
				}
			}
		}
		ws2812fx.setMode(next_mode);
		Serial.print("mode is ");
		Serial.println(ws2812fx.getModeName(ws2812fx.getMode()));
		auto_last_change = now;
	}

//  if(i-- < 0) {
//	  i = 10000;
////	  Serial.println(WiFi.localIP());
//  }
}

/*
 * Connect to WiFi. If no connection is made within WIFI_TIMEOUT, ESP gets resettet.
 */
void wifi_setup() {
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.begin(ssid, password);
	WiFi.mode(WIFI_STA);
#ifdef STATIC_IP
	WiFi.config(ip, gateway, subnet);
#endif

	unsigned long connect_start = millis();
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");

		if (millis() - connect_start > WIFI_TIMEOUT) {
			Serial.println();
			Serial.print("Tried ");
			Serial.print(WIFI_TIMEOUT);
			Serial.print("ms. Resetting ESP now.");
			ESP.reset();
		}
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
	Serial.println();
}

/*
 * Build <li> string for all modes.
 */
void data_setup() {
	data = "\"modes\": {";
	uint8_t num_modes =
			sizeof(myModes) > 0 ? sizeof(myModes) : ws2812fx.getModeCount();
	for (uint8_t i = 0; i < num_modes; i++) {
		uint8_t m = sizeof(myModes) > 0 ? myModes[i] : i;
		data = data + "\""+i+"\":\"" + ws2812fx.getModeName(m) + "\"";

		if (i+1 < num_modes) {
			data += ",";
		}
	}
	data += "},";
	data = data + "\"mode\":\"" + ws2812fx.getMode() + "\",";
	data = data + "\"speed\":\"" + ws2812fx.getSpeed() + "\",";
	data = data + "\"brightness\":\"" + brightness + "\",";
	data = data + "\"color\":" + color + ",";
	data = data + "\"power\":" + (power ? "true" : "false");
	data = "{" + data + "}";
}

/* #####################################################
 #  Webserver Functions
 ##################################################### */

void srv_handle_not_found() {
	server.send(404, "text/plain", "File Not Found");
}

void srv_handle_index_html() {
	server.send_P(200, "text/html", index_html);
}

void srv_handle_main_js() {
	server.send_P(200, "application/javascript", main_js);
}

void srv_handle_data() {
	server.send(200, "application/json", data);
}

void srv_handle_set() {
	for (uint8_t i = 0; i < server.args(); i++) {
		if (server.argName(i) == "c") {
			uint32_t tmp = (uint32_t) strtol(&server.arg(i)[0], NULL, 16);
			if (tmp >= 0x000000 && tmp <= 0xFFFFFF) {
				color = tmp;
				ws2812fx.setColor(color);
			}
		}

		if (server.argName(i) == "m") {
			uint8_t tmp = (uint8_t) strtol(&server.arg(i)[0], NULL, 10);
			ws2812fx.setMode(tmp % ws2812fx.getModeCount());
			Serial.print("mode is ");
			Serial.println(ws2812fx.getModeName(ws2812fx.getMode()));
		}

		if (server.argName(i) == "b") {
			if (server.arg(i)[0] == '-') {
				brightness *= 0.8;
			} else if (server.arg(i)[0] == ' ') {
				brightness = min(max(brightness, 5) * 1.2, 255);
			} else { // set brightness directly
				uint8_t tmp = (uint8_t) strtol(&server.arg(i)[0], NULL, 10);
				brightness = tmp;
			}

			if (power) {
				ws2812fx.setBrightness(brightness);
			} else {
				ws2812fx.setBrightness(0);
			}

			Serial.print("brightness is ");
			Serial.println(brightness);
		}

		if (server.argName(i) == "s") {
//			if (server.arg(i)[0] == '-') {
//				ws2812fx.setSpeed(ws2812fx.getSpeed() * 0.8);
//			} else {
//				ws2812fx.setSpeed(max(ws2812fx.getSpeed(), 5) * 1.2);
//			}


			ws2812fx.setSpeed((uint16_t) strtol(&server.arg(i)[0], NULL, 10));

			Serial.print("speed is ");
			Serial.println(ws2812fx.getSpeed());
		}

		if (server.argName(i) == "en") {
			if (server.arg(i) == "true") {
				power = true;
				ws2812fx.setBrightness(brightness);
			} else {
				power = false;
				ws2812fx.setBrightness(0);
			}

			Serial.print("power is ");
			Serial.println(power);
		}

//		if (server.argName(i) == "a") {
//			if (server.arg(i)[0] == '-') {
//				auto_cycle = false;
//			} else {
//				auto_cycle = true;
//				auto_last_change = 0;
//			}
//		}
	}

	data_setup();

	server.send(200, "text/plain", "OK");
}
