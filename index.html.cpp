#include <pgmspace.h>
char index_html[] PROGMEM = R"=====(
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <meta name='viewport' content='width=device-width'/>

    <title>Salt Lamp</title>

    <script type='text/javascript' src='main.js'></script>
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/@jaames/iro/dist/iro.min.js'></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/bootstrap-slider.min.js"
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/css/bootstrap-slider.min.css"
          icrossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css"
          rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>

    <style>
        .slider-selection {
            background: #BABABA;
        }

        .tooltip.in {
            opacity: 1;
        }

        .slider.slider-horizontal {
            width: 100%;
        }
    </style>
</head>

<body>
<div class="container ">
    <div class="card border-success">
        <h5 class="card-header text-white bg-success">Salt Lamp</h5>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="enabled"><h5>Power</h5></label>
                        <input id="enabled" type="checkbox" checked data-toggle="toggle" data-onstyle="danger"
                               data-size="lg">
                    </div>
                </div>
            </div>

            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-outline-primary btn-lg btn-block" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Show Color Picker
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <div class="">
                                <div class="colorPicker"></div>
                            </div>
                            <div class="">
                                <span class="title">Selected Color:</span>
                                <div id="values"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <form>
                        <div class="form-group">
                            <label for="mode">Mode</label>
                            <select class="form-control" size="10" id="mode">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="brightness">Brightness</label>
                            <input id="brightness" class="slider form-control" data-slider-id='brightnessSlider'
                                   type="text"
                                   data-slider-min="10" data-slider-max="255" data-slider-step="1"/>
                        </div>
                        <div class="form-group">
                            <label for="speed">Speed</label>
                            <input id="speed" class="slider form-control" data-slider-id='speedSlider' type="text"
                                   data-slider-min="100" data-slider-max="10000" data-slider-step="10"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
)=====";

