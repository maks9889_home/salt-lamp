#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2019-06-01 16:44:19

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <WS2812FX.h>
#include <ESP8266WebServer.h>
#include "settings.h"

void setup() ;
void loop() ;
void wifi_setup() ;
void data_setup() ;
void srv_handle_not_found() ;
void srv_handle_index_html() ;
void srv_handle_main_js() ;
void srv_handle_data() ;
void srv_handle_set() ;

#include "salt_lamp.ino"


#endif
