#include <pgmspace.h>
char main_js[] PROGMEM = R"=====(
window.addEventListener('load', setup);

function submitVal(name, val) {
    var xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'set?' + name + '=' + val, true);
    xhttp.send();
}

function compToHex(c) {
    hex = c.toString(16);
    return hex.length == 1 ? '0' + hex : hex;
}

// Thanks to the backup at http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c
function rgbToHsl(r, g, b) {
    r = r / 255;
    g = g / 255;
    b = b / 255;
    var max = Math.max(r, g, b);
    var min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;
    if (max == min) {
        h = s = 0;
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }
        h = h / 6;
    }
    return [h, s, l];
}

function setData(data, colorPicker) {
    for (x in data.modes) {
        document.getElementById("mode").innerHTML += "<option value='" + x + "'>" + data.modes[x] + "</option>";
    }

    $("#speed").bootstrapSlider('setValue', data.speed);

    $("#brightness").bootstrapSlider('setValue', data.brightness);

    $("#mode").val(data.mode);

    $("#enabled").prop('checked', data.power).change();
	colorPicker.color.hexString = compToHex(data.color);
	console.log(compToHex(data.color));
	console.log(colorPicker.color.hexString);

}

function setup() {
    // Create a new color picker instance
    // https://iro.js.org/guide.html#getting-started
    var colorPicker = new iro.ColorPicker(".colorPicker", {
        // color picker options
        // Option guide: https://iro.js.org/guide.html#color-picker-options
        width: 460,
        borderWidth: 1,
        borderColor: "#000",
    });

    var values = document.getElementById("values");

    // https://iro.js.org/guide.html#color-picker-events
    colorPicker.on(["color:change"], function (color) {
        submitVal('c', color.hexString.substr(1));
        // Show the current color in different formats
        // Using the selected color: https://iro.js.org/guide.html#selected-color-api
        values.innerHTML = [
            "hex: " + color.hexString,
            "rgb: " + color.rgbString,
            // "hsl: " + color.hslString,
        ].join("<br>");
    });

    $('.slider').bootstrapSlider({
        formatter: function (value) {
            return 'value: ' + value;
        },
    });



    $('#mode').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        submitVal('m', valueSelected)
    });

    $('#brightness').on('change', function (e) {
        var valueSelected = this.value;
        submitVal('b', valueSelected)
    });

    $('#speed').on('change', function (e) {
        var valueSelected = this.value;
        submitVal('s', valueSelected)
    });

    $('#enabled').on('change', function (e) {
        var valueSelected = this.checked;
        submitVal('en', valueSelected)
    });


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var respText = xhttp.responseText;//'{"1": "Static", "2": "Breath", "3": "Color Wipe"}';

            console.log(respText);
            var data = JSON.parse(respText);
            console.log(data);
            setData(data, colorPicker);
        }
    };
    xhttp.open('GET', 'data', true);
    xhttp.send();

    //test
    if(false) {
        respText = '{"modes":{"1": "Static", "2": "Breath", "3": "Color Wipe"}, "mode": 1, "brightness": 160, "speed": 1100, "power": false}';

        var data = JSON.parse(respText);
        console.log(data);
        setData(data);
    }
}
)=====";

